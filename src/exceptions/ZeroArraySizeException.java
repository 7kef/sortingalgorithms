package exceptions;

public class ZeroArraySizeException extends Exception {
	public ZeroArraySizeException() {
		super();
	}
	public ZeroArraySizeException(String s) {
		super(s);
	}
}
