package sorting.algorithms;
import java.lang.reflect.Type;

import exceptions.ZeroArraySizeException;

public class BubbleSort extends MainSort {
	public BubbleSort(){
		super();
	}
	
	public BubbleSort(int length) throws ZeroArraySizeException{
		super(length);
	}
	public BubbleSort(int length, Type typeClass, int leftBoundry, int rightBoundry) throws ZeroArraySizeException{
		super(length, typeClass, leftBoundry, rightBoundry);
	}

	public BubbleSort(double[] arrtosort) throws ZeroArraySizeException{
		super(arrtosort);
	}
	
	public BubbleSort(int[] arrtosort) throws ZeroArraySizeException{
		super(arrtosort);
	}
	
	/*BubbleSort(String fileName){
		super(fileName);
	}
*/
	public void sort() {
		int sorted=length;
		if (checkArray() == "arrD") {
			
			for(int j=1; j<=length-1;j++) {
				for(int i=0;i<sorted-1;i++) {
					if(arrayToSortDouble[i] > arrayToSortDouble[i+1]) {
						swap(i, i+1);
					}
				}
				sorted--;
			}
			this.endTime = System.nanoTime();
		}else {
			if(checkArray() == "arrI") {
				this.startTime = System.nanoTime();
				for(int j=1; j<=length-1;j++) {
					for(int i=0;i<sorted-1;i++) {
						if(getArrayToSortInt()[i] > getArrayToSortInt()[i+1]) {
							swap(i, i+1);
						}
					}
					sorted--;
				}

			}
		}

	}
}
