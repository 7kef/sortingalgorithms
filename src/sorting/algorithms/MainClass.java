package sorting.algorithms;
import exceptions.ZeroArraySizeException;
import sorting.algorithms.*;
public class MainClass {

	public static void main(String[] args) throws ZeroArraySizeException {
		// TODO Auto-generated method stub
		
		System.out.println("\nMerge Sort\n");
		MergeSort msort = new MergeSort();
		msort.printTable();
		msort.sort(msort.left, msort.right);
		//System.out.println("\n**************************************************\n");
		msort.printTable();
		
		System.out.println("\n\n**************************************************");
		System.out.println("**************************************************\n");
		
		
		System.out.println("\nInsertion Sort\n");
		InsertionSort isort = new InsertionSort();
		isort.printTable();
		isort.sort();
		//System.out.println("\n**************************************************\n");
		isort.printTable();

		System.out.println("\n\n**************************************************");
		System.out.println("**************************************************\n");
		
		
		System.out.println("\nBubble Sort\n");
		BubbleSort bsort = new BubbleSort();
		bsort.printTable();
		bsort.sort();
		//System.out.println("\n**************************************************\n");
		bsort.printTable();
		
		
		System.out.println("\n\n**************************************************");
		System.out.println("**************************************************\n");
		
		
		System.out.println("\nQuick Sort\n");
		QuickSort qsort = new QuickSort();
		qsort.printTable();
		qsort.sort(qsort.left, qsort.right);
		//System.out.println("\n**************************************************\n");
		qsort.printTable();
		
		
		
		
		
	}

}
