package sorting.algorithms;
import java.lang.reflect.Type;

import exceptions.ZeroArraySizeException;

public class QuickSort extends MainSort {
	public QuickSort(){
		super();
	}
	
	public QuickSort(int length) throws ZeroArraySizeException{
		super(length);
	}
	
	public QuickSort(int length, Type type, int leftBoundry, int rightBoundry) throws ZeroArraySizeException{
		super(length, type, leftBoundry, rightBoundry);
	}

	public QuickSort(double[] arrtosort) throws ZeroArraySizeException{
		super(arrtosort);
	}
	
	public QuickSort(int[] arrtosort) throws ZeroArraySizeException{
		super(arrtosort);
	}
	
	/*QuickSort(String fileName){
		super(fileName);
	}
	*/
	public void sort(int left, int right) {
		if (left < right) {
			//int middle = (left/right)/2;
			int p = partition(left, right);
			sort(left, p-1);
			sort(p+1, right);
		}
	}
	
	protected int partition(int left, int right) {
		if(checkArray() == "arrD") {
			double pivot = arrayToSortDouble[right];
			int wall = left - 1;
			for(int j=left; j<=right-1; j++) {
				if (arrayToSortDouble[j] <= pivot) {
					swap(wall+1,j);
					wall++;
				}			
			}
			swap(wall+1, right);
			return wall+1;
		}else {
			if(checkArray() == "arrI") {
				int pivot = getArrayToSortInt()[right];
				int i = left - 1;
				for(int j=left; j<=right-1; j++) {
					if (getArrayToSortInt()[j] <= pivot) {
						swap(i+1,j);
						i++;
					}			
				}
				swap(i+1, right);
				return i+1;
			}
		}
		return -1;
	}
}
