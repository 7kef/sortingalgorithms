package sorting.algorithms;
import java.lang.reflect.Type;
import java.text.DecimalFormat;
import java.util.Arrays;
import java.util.concurrent.ThreadLocalRandom;

import exceptions.ZeroArraySizeException;

public class MainSort {
	protected double[] arrayToSortDouble = null;
	protected int length=0, left=0, right=0;
	private int[] arrayToSortInt = null;
	
	protected long startTime = 0;
	protected long endTime = 0;
	protected long duration = 0;
	
	public int leftBoundry, rightBoundry;
	
	public MainSort(){
		
		//randoming length of array
		this.length = ThreadLocalRandom.current().nextInt(5,20);
		//initialization of int array
		try {
			setArrayToSortInt(new int[this.length]);
		} catch (ZeroArraySizeException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		
		for(int i=0; i<this.length; i++) {
			//randoming insert value
			int k=ThreadLocalRandom.current().nextInt(0,50);
			
				//generating uniq elements
				for(int element : getArrayToSortInt()) {
					while(element==k)
						k = ThreadLocalRandom.current().nextInt(0,50);
				}
			
			getArrayToSortInt()[i] = k;
			}
		//last index od array
		this.right = this.getArrayToSortInt().length-1;
	}
	
	public MainSort(int length) throws ZeroArraySizeException{
		if(length == 0)
			throw new ZeroArraySizeException();
			
		this.length = length;
		arrayToSortDouble = new double[(int) this.length];
		for(int i=0; i<this.length; i++) {
			arrayToSortDouble[i] = ThreadLocalRandom.current().nextDouble(1, 1000);
		}
		this.right = this.arrayToSortDouble.length-1;
	}
	
	public MainSort(int length, Type typeClass, int leftBoundry, int rightBoundry) throws ZeroArraySizeException{
		
		if(length == 0)
			throw new ZeroArraySizeException();
			
		this.length = length;
		if(typeClass==Double.TYPE) {
			arrayToSortDouble = new double[this.length];
			for(int i=0; i<this.length; i++) {
				arrayToSortDouble[i] = ThreadLocalRandom.current().nextDouble(leftBoundry, rightBoundry);
			}
			this.right = this.arrayToSortDouble.length-1;
		}else {
			if(typeClass==Integer.TYPE) {
				arrayToSortInt = new int[this.length];
				for(int i=0; i<this.length; i++) {
					arrayToSortInt[i] = ThreadLocalRandom.current().nextInt(leftBoundry, rightBoundry);
				}
				this.right = this.arrayToSortInt.length-1;
			}
		}
	}
	
	
	
	public MainSort(double[] arrtosort) throws ZeroArraySizeException{
		if(arrtosort.length == 0) throw new ZeroArraySizeException();
		this.arrayToSortDouble = arrtosort;
		this.length = arrayToSortDouble.length;
		this.right = this.length-1;
	}
	
	public MainSort(int[] arrtosort) throws ZeroArraySizeException{
		if(arrtosort.length == 0) throw new ZeroArraySizeException();
		this.setArrayToSortInt(arrtosort);
		this.length = getArrayToSortInt().length;
		this.right = this.length-1;
	}
	
	/*MainSort(String fileName){
		try(FileInputStream fis = new FileInputStream(fileName)){
			int bufferSize=1024,c;
			byte[] buffer=new byte[bufferSize];
			while((c=fis.read(buffer, 0, bufferSize))>-1){
				fis.read(buffer, 0, c);
			}
			
		} catch (FileNotFoundException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}*/
	
	public void printTable() {
		if(checkArray() =="arrD") {
			System.out.print("ArrayDouble : ");
			System.out.print(Arrays.toString(arrayToSortDouble));
			System.out.println();
		}
		else {
			if(checkArray()=="arrI") {
				System.out.print("ArrayInteger : ");
				System.out.print(Arrays.toString(getArrayToSortInt()));
				System.out.println();
			}
		}
	}
	
	
	//mergesort
	//protected abstract void sort(int left, int right);
	
	public void printTable(double[] arr) {
		System.out.println(Arrays.toString(arr));
	}
	public void printTable(int[] arr) {
		System.out.println(Arrays.toString(arr));
	}
	
	protected void swap(int index1, int index2) {
		if(checkArray() == "arrD") {
			double temp = arrayToSortDouble[index1];
			arrayToSortDouble[index1] = arrayToSortDouble[index2];
			arrayToSortDouble[index2] = temp;
		}else {
			if(checkArray() == "arrI") {
				int temp = getArrayToSortInt()[index1];
				getArrayToSortInt()[index1] = getArrayToSortInt()[index2];
				getArrayToSortInt()[index2] = temp;
			}
		}
	}
	
	public String checkArray() {
		if(arrayToSortDouble!=null)
			return "arrD";
		if(getArrayToSortInt()!=null)
			return "arrI";
		
		return "ArrayIsNotInitialized";
	}
	
	public void setDuration() {
		this.duration = this.endTime - this.startTime;
	}
	public long getDuration() {
		return this.duration;
	}
	public String getStringDuration() {
		double seconds = (double)this.duration/1E9;
		return new DecimalFormat("#.################################").format(seconds)+" sec";
	}

	public int[] getArrayToSortInt() {
		return arrayToSortInt;
	}

	public void setArrayToSortInt(int[] is) throws ZeroArraySizeException {
		if (is.length == 0)
				throw new ZeroArraySizeException();
		this.arrayToSortInt = is;
	}

	public double[] getArrayToSortDouble() {
		return arrayToSortDouble;
	}

	public void setArrayToSortDouble(double[] arrayToSortDouble) {
		this.arrayToSortDouble = arrayToSortDouble;
	}

	public int getLength() {
		return length;
	}

	public int getLeft() {
		return left;
	}

	public int getRight() {
		return right;
	}

	public void setStartTime(long startTime) {
		this.startTime = startTime;
	}

	public void setEndTime(long endTime) {
		this.endTime = endTime;
	}

	public long getStartTime() {
		return startTime;
	}

	public long getEndTime() {
		return endTime;
	}

	

	
}
