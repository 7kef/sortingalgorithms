package sorting.algorithms;
import java.lang.reflect.Type;

import exceptions.ZeroArraySizeException;

public class InsertionSort extends MainSort {
	public InsertionSort(){
		super();
	}
	
	public InsertionSort(int length) throws ZeroArraySizeException{
		super(length);
	}
	
	public InsertionSort(int length, Type typeClass, int leftBoundry, int rightBoundry) throws ZeroArraySizeException{
		super(length, typeClass,leftBoundry,rightBoundry);
	}

	public InsertionSort(double[] arrtosort) throws ZeroArraySizeException{
		super(arrtosort);
	}
	
	public InsertionSort(int[] arrtosort) throws ZeroArraySizeException{
		super(arrtosort);
	}
	
	
	/*InsertionSort(String fileName){
		super(fileName);
	}*/
	
	
	public void sort(){
		
		if (checkArray() == "arrD") {
			for(int index = 1; index<length; index++) {
				for(int i=index; i>0; i--) {
					if(arrayToSortDouble[i-1] > arrayToSortDouble[i]) {
						swap(i-1, i);
					}
					else break;
				}
			}
			this.endTime = System.nanoTime();
		}
		else {
			if (checkArray() == "arrI") {
				this.startTime = System.nanoTime();
				for(int index = 1; index<length; index++) {
					for(int i=index; i>0; i--) {
						if(getArrayToSortInt()[i-1] > getArrayToSortInt()[i]) {
							swap(i-1, i);
						}
						else break;
					}
				}

			}
		}
		
	}
	

	
}
