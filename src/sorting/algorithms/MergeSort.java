package sorting.algorithms;
import java.lang.reflect.Type;

import exceptions.ZeroArraySizeException;

public class MergeSort extends MainSort{
	//private int length=0, left=0, right=0;
	
	public MergeSort(){
		super();
	}
	
	public MergeSort(int length) throws ZeroArraySizeException{
		super(length);
	}
	
	public MergeSort(int length, Type type, int leftBoundry, int rightBoundry) throws ZeroArraySizeException{
		super(length, type, leftBoundry, rightBoundry);
	}

	public MergeSort(double[] arrtosort) throws ZeroArraySizeException{
		super(arrtosort);
	}
	
	public MergeSort(int[] arrtosort) throws ZeroArraySizeException{
		super(arrtosort);
	}
	
	/*MergeSort(String fileName){
		super(fileName);
	}*/
	
	
	//sort = true - ascending, false - descending
	public void sort(int left, int right) {
		//recursively run while left<right
		
		if(left<right) {

			int m = (left+right)/2;
			sort(left, m);
			sort(m+1, right);
			merge(left, m, right);
		}
			
	}
	
	protected void merge(int left, int middle, int right) {
		
		if(checkArray() == "arrD") {
			//creating 2 aditional arrays for left and right parts
			double[] arrL = new double[middle-left+1];
			double[] arrR = new double[right-middle];
			//copying values from sorting array to left array
			int w=0;
			for(int i=left; i<=middle; i++) {
				arrL[w] = arrayToSortDouble[i];
				w++;
			}
			//copying values from sorting array to right array
			int q=0;
			for(int j=middle+1; j<=right; j++) {
				arrR[q] = arrayToSortDouble[j];
				q++;
			}
			//variables to iterate through arrL and arrR 
			int li=0, ri=0;
			//writing to sorting array proper values after comparing
			for(int k=left; k<=right; k++) {
				if(li<arrL.length) {
					if(ri==arrR.length) {
						arrayToSortDouble[k] = arrL[li];
						li++;
					}
					else {
						if(arrL[li]<arrR[ri]) {
							arrayToSortDouble[k] = arrL[li];
							li++;
						}
						else {
								arrayToSortDouble[k] = arrR[ri];
								ri++;
						}
					}
				}
			}
					
		}
		
		if(checkArray() == "arrI") {
			//creating 2 aditional arrays for left and right parts
			int[] arrL = new int[middle-left+1];
			int[] arrR = new int[right-middle];
			//copying values from sorting array to left array
			int w=0;
			for(int i=left; i<=middle; i++) {
				arrL[w] = getArrayToSortInt()[i];
				w++;
			}
			//copying values from sorting array to right array
			int q=0;
			for(int j=middle+1; j<=right; j++) {
				arrR[q] = getArrayToSortInt()[j];
				q++;
			}
			//variables to iterate through arrL and arrR 
			int li=0, ri=0;
			//writing to sorting array proper values after comparing
			for(int k=left; k<=right; k++) {
				if(li<arrL.length) {
					if(ri==arrR.length) {
						getArrayToSortInt()[k] = arrL[li];
						li++;
					}
					else {
						if(arrL[li]<arrR[ri]) {
							getArrayToSortInt()[k] = arrL[li];
							li++;
						}
						else {
								getArrayToSortInt()[k] = arrR[ri];
								ri++;
						}
					}
				}
			}
					
		}
	
		
	}

	
	

//koniec klasy
}
