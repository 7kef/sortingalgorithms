package fx.models;

import javafx.beans.property.*;
import javafx.beans.property.IntegerProperty;
import javafx.beans.property.SimpleBooleanProperty;
import javafx.beans.property.SimpleIntegerProperty;
import javafx.beans.property.SimpleStringProperty;
import javafx.beans.property.StringProperty;

public class MainScreenModel {
	
	//selected item from menuSelector
	private StringProperty algorithmProperty = new SimpleStringProperty();
	//selected number type in Tooggle Radio 
	private StringProperty numberType = new SimpleStringProperty();
	//flag Integer selected
	private BooleanProperty integerSelected = new SimpleBooleanProperty();
	//flag Double selected
	private BooleanProperty doubleSelected = new SimpleBooleanProperty();
	//table size
	private StringProperty tableSizeProprety = new SimpleStringProperty();
	
	public MainScreenModel() {
		//algorithmProperty.bind();
		
		
	}

	public StringProperty getAlgorithmProperty() {
		return algorithmProperty;
	}

	public StringProperty getNumberType() {
		return numberType;
	}

	public BooleanProperty getIntegerSelected() {
		return integerSelected;
	}
	
	public BooleanProperty getDoubleSelected() {
		return doubleSelected;
	}

	public StringProperty getTableSizeProprety() {
		return tableSizeProprety;
	}

	
	
	
	
	
}
