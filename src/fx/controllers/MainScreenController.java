package fx.controllers;

import java.lang.reflect.InvocationTargetException;
import java.lang.reflect.Type;
import java.text.DecimalFormat;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.concurrent.TimeUnit;

import fx.models.MainScreenModel;
import javafx.beans.binding.Binding;
import javafx.beans.binding.Bindings;
import javafx.beans.binding.NumberBinding;
import javafx.beans.property.IntegerProperty;
import javafx.beans.property.ReadOnlyObjectProperty;
import javafx.beans.property.SimpleIntegerProperty;
import javafx.beans.property.SimpleStringProperty;
import javafx.beans.value.ChangeListener;
import javafx.beans.value.ObservableValue;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.scene.control.Button;
import javafx.scene.control.ChoiceBox;
import javafx.scene.control.Label;
import javafx.scene.control.ListView;
import javafx.scene.control.ProgressIndicator;
import javafx.scene.control.RadioButton;
import javafx.scene.control.TableColumn;
import javafx.scene.control.TableView;
import javafx.scene.control.TextField;
import javafx.scene.control.Toggle;
import javafx.scene.control.ToggleGroup;
import javafx.scene.control.Tooltip;
import javafx.scene.input.ContextMenuEvent;
import javafx.scene.input.InputMethodEvent;
import javafx.scene.layout.HBox;
import javafx.util.StringConverter;
import javafx.util.converter.IntegerStringConverter;
import javafx.util.converter.NumberStringConverter;
import sorting.algorithms.BubbleSort;
import sorting.algorithms.InsertionSort;
import sorting.algorithms.MergeSort;
import sorting.algorithms.QuickSort;

public class MainScreenController {

	@FXML
	private ChoiceBox<String> menuSelector;

	@FXML
	private TextField tableSize;
	
	
	@FXML
    private TextField leftBoundryText;

    @FXML
    private TextField rightBoundryText;

	@FXML
	private RadioButton integerRadio;

	@FXML
	private ToggleGroup numberType;

	@FXML
	private RadioButton doubleRadio;

	@FXML
	private Button sort;

	@FXML
	private Button tableInitialization;

	@FXML
	private ListView<?> sortingTable;

	@FXML
	private Label sortingAlgorithmSummary;

	@FXML
	private Label tableSizeSummary;

	@FXML
	private Label numberTypeIntegerSummary;

	@FXML
	private Label numberTypeDoubleSummary;

	@FXML
    private Label leftBoundrySummary;

    @FXML
    private Label rightBoundrySummary;
	
	@FXML
	private HBox sortingSummary;


	@FXML
	private Label sortingTimeSummary;
	
	@FXML
    private Label stateLabel;
	
	


	private Map<String, Class<?>> algorithmClassMap;

	private MainScreenModel mainScreenModel = new MainScreenModel();

	// variables for algorithms
	private InsertionSort insertionSort = null;
	private BubbleSort bubbleSort = null;
	private MergeSort mergeSort = null;
	private QuickSort quickSort = null;

	

	public MainScreenController() {
		algorithmClassMap = algorithmInitialization();
	}

	@FXML
	public void initialize() {
		menuSelectorInit();
		// binding selected value from menuSelector with AlgorithmProperty
		// menuSelector.valueProperty().bind(mainScreenModel.getAlgorithmProperty());
		mainScreenModel.getAlgorithmProperty().bind(menuSelector.valueProperty());

		integerRadio.setUserData("Integer");
		doubleRadio.setUserData("Double");

		// binding integerRadio selected with BoolProperty integer Selected
		mainScreenModel.getIntegerSelected().bind(integerRadio.selectedProperty());
		// integerRadio.selectedProperty().bind(mainScreenModel.getIntegerSelected());

		// binding doubleRadio selected with BoolProperty dobuble Selected
		mainScreenModel.getDoubleSelected().bind(doubleRadio.selectedProperty());
		// doubleRadio.selectedProperty().bind(mainScreenModel.getDoubleSelected());

		// binding to table size property
		mainScreenModel.getTableSizeProprety().bind(tableSize.textProperty());
		// tableSize.textProperty().bind(mainScreenModel.getTableSizeProprety().asString());

		// binding summary to actual choosen values
		sortingAlgorithmSummary.textProperty().bind(mainScreenModel.getAlgorithmProperty());
		tableSizeSummary.textProperty().bind(mainScreenModel.getTableSizeProprety());
		numberTypeIntegerSummary.visibleProperty().bind(mainScreenModel.getIntegerSelected());
		numberTypeDoubleSummary.visibleProperty().bind(mainScreenModel.getDoubleSelected());
		leftBoundrySummary.textProperty().bind(leftBoundryText.textProperty());
		rightBoundrySummary.textProperty().bind(rightBoundryText.textProperty());


	}

	@FXML
	public void randomTable(ActionEvent event) {
		sortingSummary.visibleProperty().set(false);
		tableInitialization.disableProperty().set(true);
		stateLabel.textProperty().set("Initializing table...");
		String sortAlg = mainScreenModel.getAlgorithmProperty().getValue();
		int tableLength = Integer.valueOf(mainScreenModel.getTableSizeProprety().getValue());
		boolean integerType = mainScreenModel.getIntegerSelected().getValue();
		boolean doubleType = mainScreenModel.getDoubleSelected().getValue();
		int leftBoundry = Integer.valueOf(leftBoundryText.textProperty().getValue());
		int rightBoundry = Integer.valueOf(rightBoundryText.textProperty().getValue());
		Type currentType = integerType ? Integer.TYPE : Double.TYPE;
		fillTable(sortAlg, tableLength, currentType, leftBoundry, rightBoundry);
		tableInitialization.disableProperty().set(false);
		stateLabel.textProperty().set("Ready");
	}

	@FXML
	public void sortTable(ActionEvent event) {
		stateLabel.textProperty().set("Sorting table...");
		tableInitialization.disableProperty().set(true);
		sort.disableProperty().set(true);
		String duration = "";
		if (insertionSort != null && mainScreenModel.getAlgorithmProperty().getValue()=="Insertion Sort") {
			insertionSort.setStartTime(System.nanoTime());
			insertionSort.sort();
			insertionSort.setEndTime(System.nanoTime());
			insertionSort.setDuration();
			duration = insertionSort.getStringDuration();
		}else { if (bubbleSort != null && mainScreenModel.getAlgorithmProperty().getValue()=="Bubble Sort") {
						bubbleSort.setStartTime(System.nanoTime());
						bubbleSort.sort();
						bubbleSort.setEndTime(System.nanoTime());
						bubbleSort.setDuration();
						duration = bubbleSort.getStringDuration();
				}
				else if (mergeSort != null && mainScreenModel.getAlgorithmProperty().getValue()=="Merge Sort") {
					mergeSort.setStartTime(System.nanoTime());
					mergeSort.sort(mergeSort.getLeft(), mergeSort.getRight());
					mergeSort.setEndTime(System.nanoTime());
					mergeSort.setDuration();
					duration = mergeSort.getStringDuration();
				}
				else if (quickSort != null && mainScreenModel.getAlgorithmProperty().getValue()=="Quick Sort") {
					quickSort.setStartTime(System.nanoTime());
					quickSort.sort(quickSort.getLeft(), quickSort.getRight());
					quickSort.setEndTime(System.nanoTime());
					quickSort.setDuration();
					duration = quickSort.getStringDuration();
				}
		}
		refreshTable();
		sortingTimeSummary.textProperty().set(duration);
		sortingSummary.visibleProperty().set(true);
		sort.disableProperty().set(false);
		tableInitialization.disableProperty().set(false);
		stateLabel.textProperty().set("Sorted");
	}

	
	
	
	
	private void refreshTable() {
		Type currentType = mainScreenModel.getIntegerSelected().getValue() ? Integer.TYPE : Double.TYPE;
		if (insertionSort != null) {
			if (insertionSort.checkArray()=="arrD") {
				ObservableList data = FXCollections.observableArrayList();
				// copying elements from table to ObservableList
				for (double element : insertionSort.getArrayToSortDouble()) {
					data.add(element);
				}
				sortingTable.setItems(data);
			} else if (insertionSort.checkArray()=="arrI") {
				ObservableList data = FXCollections.observableArrayList();
				// copying elements from table to ObservableList
				for (int element : insertionSort.getArrayToSortInt()) {
					data.add(element);
				}
				sortingTable.setItems(data);
			}
			sortingTable.refresh();
		}else { if (mergeSort != null) {
					if (mergeSort.checkArray()=="arrD") {
						ObservableList data = FXCollections.observableArrayList();
						// copying elements from table to ObservableList
						for (double element : mergeSort.getArrayToSortDouble()) {
							data.add(element);
						}
						sortingTable.setItems(data);
					} else if (mergeSort.checkArray()=="arrI") {
						ObservableList data = FXCollections.observableArrayList();
						// copying elements from table to ObservableList
						for (int element : mergeSort.getArrayToSortInt()) {
							data.add(element);
						}
						sortingTable.setItems(data);
					}
					sortingTable.refresh();
			
			}else if (bubbleSort != null) {
				if (bubbleSort.checkArray()=="arrD") {
					ObservableList data = FXCollections.observableArrayList();
					// copying elements from table to ObservableList
					for (double element : bubbleSort.getArrayToSortDouble()) {
						data.add(element);
					}
					sortingTable.setItems(data);
				} else if (bubbleSort.checkArray()=="arrI") {
					ObservableList data = FXCollections.observableArrayList();
					// copying elements from table to ObservableList
					for (int element : bubbleSort.getArrayToSortInt()) {
						data.add(element);
					}
					sortingTable.setItems(data);
				}
				sortingTable.refresh();
				
			}else if (quickSort != null) {
				if (quickSort.checkArray()=="arrD") {
					ObservableList data = FXCollections.observableArrayList();
					// copying elements from table to ObservableList
					for (double element : quickSort.getArrayToSortDouble()) {
						data.add(element);
					}
					sortingTable.setItems(data);
				} else if (quickSort.checkArray()=="arrI") {
					ObservableList data = FXCollections.observableArrayList();
					// copying elements from table to ObservableList
					for (int element : quickSort.getArrayToSortInt()) {
						data.add(element);
					}
					sortingTable.setItems(data);
				}
				sortingTable.refresh();
			}
		}
		
	}
	
	private void fillTable(String sortAlg, int tableLength, Type currentType, int leftBounry, int rightBoundry) {
		switch (sortAlg) {
		case "Insertion Sort": {
			try {
				insertionSort = (InsertionSort) algorithmClassMap.get("insertion").getConstructor(int.class, Type.class, int.class,int.class)
						.newInstance(tableLength, currentType, leftBounry, rightBoundry);
			} catch (InstantiationException e) {
				e.printStackTrace();
			} catch (IllegalAccessException e) {
				e.printStackTrace();
			} catch (IllegalArgumentException e) {
				e.printStackTrace();
			} catch (InvocationTargetException e) {
				e.printStackTrace();
			} catch (NoSuchMethodException e) {
				e.printStackTrace();
			} catch (SecurityException e) {
				e.printStackTrace();
			}
			if (currentType == Double.TYPE) {
				ObservableList data = FXCollections.observableArrayList();
				// copying elements from table to ObservableList
				for (double element : insertionSort.getArrayToSortDouble()) {
					data.add(element);
				}
				sortingTable.setItems(data);
			} else if (currentType == Integer.TYPE) {
				ObservableList data = FXCollections.observableArrayList();
				// copying elements from table to ObservableList
				for (int element : insertionSort.getArrayToSortInt()) {
					data.add(element);
				}
				sortingTable.setItems(data);
			}
			sortingTable.refresh();
			break;
		}
		case "Bubble Sort": {

			try {
				bubbleSort = (BubbleSort) algorithmClassMap.get("bubble").getConstructor(int.class, Type.class, int.class,int.class)
						.newInstance(tableLength, currentType, leftBounry, rightBoundry);
			} catch (InstantiationException e) {
				e.printStackTrace();
			} catch (IllegalAccessException e) {
				e.printStackTrace();
			} catch (IllegalArgumentException e) {
				e.printStackTrace();
			} catch (InvocationTargetException e) {
				e.printStackTrace();
			} catch (NoSuchMethodException e) {
				e.printStackTrace();
			} catch (SecurityException e) {
				e.printStackTrace();
			}
			if (currentType == Double.TYPE) {
				ObservableList data = FXCollections.observableArrayList();
				// copying elements from table to ObservableList
				for (double element : bubbleSort.getArrayToSortDouble()) {
					data.add(element);
				}
				sortingTable.setItems(data);
			} else if (currentType == Integer.TYPE) {
				ObservableList data = FXCollections.observableArrayList();
				// copying elements from table to ObservableList
				for (int element : bubbleSort.getArrayToSortInt()) {
					data.add(element);
				}
				sortingTable.setItems(data);
			}
			sortingTable.refresh();
			break;
		}
		case "Merge Sort": {
			try {
				mergeSort = (MergeSort) algorithmClassMap.get("merge").getConstructor(int.class, Type.class, int.class,int.class)
						.newInstance(tableLength, currentType, leftBounry, rightBoundry);
			} catch (InstantiationException e) {
				e.printStackTrace();
			} catch (IllegalAccessException e) {
				e.printStackTrace();
			} catch (IllegalArgumentException e) {
				e.printStackTrace();
			} catch (InvocationTargetException e) {
				e.printStackTrace();
			} catch (NoSuchMethodException e) {
				e.printStackTrace();
			} catch (SecurityException e) {
				e.printStackTrace();
			}
			if (currentType == Double.TYPE) {
				ObservableList data = FXCollections.observableArrayList();
				// copying elements from table to ObservableList
				for (double element : mergeSort.getArrayToSortDouble()) {
					data.add(element);
				}
				sortingTable.setItems(data);
			} else if (currentType == Integer.TYPE) {
				ObservableList data = FXCollections.observableArrayList();
				// copying elements from table to ObservableList
				for (int element : mergeSort.getArrayToSortInt()) {
					data.add(element);
				}
				sortingTable.setItems(data);
			}
			sortingTable.refresh();
			break;
		}
		case "Quick Sort": {
			try {
				quickSort = (QuickSort) algorithmClassMap.get("quick").getConstructor(int.class, Type.class, int.class,int.class)
						.newInstance(tableLength, currentType, leftBounry, rightBoundry);
			} catch (InstantiationException e) {
				e.printStackTrace();
			} catch (IllegalAccessException e) {
				e.printStackTrace();
			} catch (IllegalArgumentException e) {
				e.printStackTrace();
			} catch (InvocationTargetException e) {
				e.printStackTrace();
			} catch (NoSuchMethodException e) {
				e.printStackTrace();
			} catch (SecurityException e) {
				e.printStackTrace();
			}
			if (currentType == Double.TYPE) {
				ObservableList data = FXCollections.observableArrayList();
				// copying elements from table to ObservableList
				for (double element : quickSort.getArrayToSortDouble()) {
					data.add(element);
				}
				sortingTable.setItems(data);
			} else if (currentType == Integer.TYPE) {
				ObservableList data = FXCollections.observableArrayList();
				// copying elements from table to ObservableList
				for (int element : quickSort.getArrayToSortInt()) {
					data.add(element);
				}
				sortingTable.setItems(data);
			}
			sortingTable.refresh();
			break;
		}
		default: {
			QuickSort quickSort = null;
			try {
				quickSort = (QuickSort) algorithmClassMap.get("quick").getConstructor(int.class, Type.class, int.class,int.class)
						.newInstance(tableLength, currentType, leftBounry, rightBoundry);
			} catch (InstantiationException e) {
				e.printStackTrace();
			} catch (IllegalAccessException e) {
				e.printStackTrace();
			} catch (IllegalArgumentException e) {
				e.printStackTrace();
			} catch (InvocationTargetException e) {
				e.printStackTrace();
			} catch (NoSuchMethodException e) {
				e.printStackTrace();
			} catch (SecurityException e) {
				e.printStackTrace();
			}
			if (currentType == Double.TYPE) {
				ObservableList data = FXCollections.observableArrayList();
				// copying elements from table to ObservableList
				for (double element : quickSort.getArrayToSortDouble()) {
					data.add(element);
				}
				sortingTable.setItems(data);
			} else if (currentType == Integer.TYPE) {
				ObservableList data = FXCollections.observableArrayList();
				// copying elements from table to ObservableList
				for (int element : quickSort.getArrayToSortInt()) {
					data.add(element);
				}
				sortingTable.setItems(data);
			}
			sortingTable.refresh();
		}

		}
	}

	public void menuSelectorInit() {
		menuSelector.setItems(
				FXCollections.observableArrayList("Bubble Sort", "Insertion Sort", "Merge Sort", "Quick Sort"));
		menuSelector.setTooltip(new Tooltip("Select the algorithm"));
		menuSelector.getSelectionModel().selectFirst();
		
	}

	public Map<String, Class<?>> algorithmInitialization() {
		Map<String, Class<?>> algorithmMap = new HashMap<>();
		algorithmMap.put("bubble", BubbleSort.class);
		algorithmMap.put("insertion", InsertionSort.class);
		algorithmMap.put("merge", MergeSort.class);
		algorithmMap.put("quick", QuickSort.class);
		return algorithmMap;
	}

}
