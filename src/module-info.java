module hellofx {
    requires javafx.controls;
    requires javafx.fxml;
    requires transitive javafx.graphics;
    
    opens fx.main to javafx.fxml;
    exports fx.main;
}