import org.junit.After;
import org.junit.Before;
import org.junit.Test;

import exceptions.ZeroArraySizeException;
import sorting.algorithms.MainSort;

public class MainSortTest {
	@Before
	public void setUp() throws Exception {
	}

	@After
	public void tearDown() throws Exception {
	}
	//default constructor
	@Test(expected = NegativeArraySizeException.class)
	public void shouldThrowNegativeArraySizeException() throws ZeroArraySizeException {
		new MainSort(-10);	
	}
	//constructor with length paramater
	@Test(expected = ZeroArraySizeException.class)
	public void shouldThrowZeroArraySizeException() throws ZeroArraySizeException {
		new MainSort(0);	
	}
	//constructor with double array parametr
	@Test(expected = ZeroArraySizeException.class)
	public void constructorWithDoubleArrayShouldThrowZeroArraySizeException() throws ZeroArraySizeException {
		double[] testarray = new double[0];
		new MainSort(testarray);	
	}
	//constructor with int array parameter
	@Test(expected = ZeroArraySizeException.class)
	public void constructorWithIntArrayShouldThrowZeroArraySizeException() throws ZeroArraySizeException {
		int[] testarray = new int[0];
		new MainSort(testarray);	
	}
	//setter for the double array
	@Test(expected = ZeroArraySizeException.class)
	public void intSetterShouldThrowZeroArrayException() throws ZeroArraySizeException{
		int[] testarray = new int[10];
		int[] testarray1 = new int[0];
		MainSort maintest = new MainSort(testarray);
		maintest.setArrayToSortInt(testarray1);
		
	}


}
