# Sorting Algorithms

### The project aim is to create my own implementation of well-known algorithms in order to learn how they work. 
### The program also shows the difference between algorithms execution time, which is the main determinant of algorithm performance.

### The project is written in **```Java```** with the use of **```JavaFX```** framework to create Graphical User Interface(GUI).

### The program is exported into an executable .jar file, which means you only need Java installed to open it. 

## If you want to get the **program itself** (without all source files) download the .jar file (e.g. SortingAlgorithms-0.1.jar) in root directory of the repository. 

### Currently there are implementations of:

1. Bubble Sort
2. Insertion Sort
3. Merge Sort
4. Quick Sort 

### Hopefully, these algorithms will be added to the program:

1. Selection Sort
2. Counting Sort
3. Heap Sort    

### And some Search Algorithms

1. Linear search
2. Binary search